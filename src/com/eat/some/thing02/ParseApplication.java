package com.eat.some.thing02;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseUser;

public class ParseApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();

		// Add your initialization code here
		 Parse.initialize(this, "TtrQRD2Tf7AK6lFHq3i8vlpbVaAhSPuyyJPwLg4n", "06UY4xvGONQD5o2pwU0WPMhK9nXUrVv71kRyu4NS"); 
		ParseUser.enableAutomaticUser();
		ParseACL defaultACL = new ParseACL();
	    
		// If you would like all objects to be private by default, remove this line.
		defaultACL.setPublicReadAccess(true);
		
		ParseACL.setDefaultACL(defaultACL, true);
	}

}
